%%%-------------------------------------------------------------------
%% @doc d_test top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(d_test_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    error_logger:info_msg("ciao"),

    SupFlags = #{strategy  => one_for_one,
        intensity => 5,
        period    => 30},

    {ok, { SupFlags, [s1_spec(),g1_spec()]} }.

s1_spec() ->
    #{id       => s1,                      % mandatory
        start    => {s1, start_link, []},    % mandatory
        restart  => permanent,                     % optional
        type     => worker}.                       % optional

g1_spec() ->
    #{id       => g1,                      % mandatory
        start    => {g1, start_link, []},    % mandatory
        restart  => permanent,                     % optional
        type     => worker}.                       % optional

%%====================================================================
%% Internal functions
%%====================================================================


%nuovo commento